import axios, { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';
import { Environment } from '../environment/environment';


const requestConfig: AxiosRequestConfig = {

    baseURL: Environment.apiBaseUrlGCP,
    headers: {}

}
const tableInstance = axios.create(requestConfig);

export default tableInstance;

