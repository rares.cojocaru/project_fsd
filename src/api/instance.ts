import axios, { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';
import { Environment } from '../environment/environment';

let headers = {}
const requestConfig:AxiosRequestConfig={

    baseURL: Environment.apiBaseUrl,
    headers: {}

}
const instance = axios.create(requestConfig);

 export default instance;

