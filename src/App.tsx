import './App.css';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import { Avatar } from '@mui/material';
import MailIcon from '@mui/icons-material/Mail';
import LockIcon from '@mui/icons-material/Lock';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { InputAdornment } from '@mui/material';
import { Component } from 'react';
import AuthContext from './Context';
import React, { useContext } from 'react';
import useAuth from './hooks/useAuth';


export const theme_bac = {
  background: "linear-gradient(45deg, #3333ff, #cc0099)",
  height: "100vh",
  width: "100vw"
};

const Login = (props: any) => {
  const authContext = useContext(AuthContext);
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  // const { push } = React.useHistory()

const onEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  setEmail(event.target.value)
}

const onPasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
}
const buttonClicked = async () => {
    useAuth.login(email, password).then(response => {
        if (response) {
            console.log(response);
            localStorage.setItem("token", response);
            props.history.push("/list");
            props.setAuth(true);
        }
    }).catch (error => {
        //alert("Incorrect login!");
    })
}
   return (
      <div className="App">
        <header className="App-header" style={{ ...theme_bac }} >
          <Card>
            <CardContent sx={{ width: '35rem' }}>

              <Grid container spacing={2} direction="row"
                justifyContent="space-evenly"
                alignItems="center">
                <Grid item>
                  <Avatar
                    alt="Remy Sharp"
                    //src="/static/images/avatar/1.jpg"
                    sx={{ width: 130, height: 130 }}
                  />
                </Grid>

                <Grid item>
                  <Stack spacing={2} alignItems="center">
                    <div>User Login</div>
                    <Box>
                      <TextField onChange={onEmailChange} value={email} required id="outlined-basic" label="Username" variant="outlined"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <MailIcon />
                            </InputAdornment>
                          )
                        }}
                      />
                    </Box>
                    <Box>
                      <TextField type="password" onChange={onPasswordChange} value={password} required id="outlined-basic" label="Password" variant="outlined"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <LockIcon />
                            </InputAdornment>
                          )
                        }}
                      />
                    </Box>
                    <CardActions>
                      <Button variant="contained" color="success" onClick={buttonClicked}>Log In</Button>
                    </CardActions>
                  </Stack>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </header>
      </div>
    ); 
}

export default Login;