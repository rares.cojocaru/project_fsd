import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Info from './list.json';
import { Component } from 'react'
import { Button } from '@mui/material';


class Page2 extends Component {

  constructor(props) {
    super(props);
    this.buttonClicked = this.buttonClicked.bind(this);
  }

  buttonClicked = () => {
    this.props.history.push('/upload');
  }

  render() {
    return (
      <div>
      <TableContainer >
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow >
              <TableCell sx={{ fontStyle: 'bold' }}>Name</TableCell>
              <TableCell align="right">Size</TableCell>
              <TableCell align="right">Recognition</TableCell>
              <TableCell align="right">Download Link</TableCell>
  
            </TableRow>
          </TableHead>
          <TableBody>
            {Info.map((row) => (
              <TableRow
                key={row.name}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.size}</TableCell>
                <TableCell align="right">{row.recognition}</TableCell>
                <TableCell align="right"><a href={row.link}>{row.link}</a></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Button variant="contained" color="success" onClick={this.buttonClicked} style={{ marginLeft: "800px", marginTop: "20px"}}>Next Page</Button>
      </div>
    );
  }
}

export default Page2;

