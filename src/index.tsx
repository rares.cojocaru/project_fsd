import React from 'react';
import { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Upload from './Upload'
import MyList from './Page2';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter,Switch,Route } from 'react-router-dom';
import AuthContext from './Context';
import DatePicker from './DatePicker/DatePicker';
import interceptors from "../src/api/interceptors";
import {createBrowserHistory} from "history";
import Login from './App';
import Table from './Table';


function AppRender(){
const history = createBrowserHistory();
const [isAuth, setAuth] = useState(false);
interceptors.setupInterceptors(history);

return(
<AuthContext.Provider value={{isAuth, setAuth}}>
<BrowserRouter>
  <Switch>
    <Route exact path="/" component={Login} />
    <Route path="/list" component={MyList} />
    <Route path="/upload" component={Upload} />
    <Route path="/datepicker" component={DatePicker} />
    <Route path="/table" component={Table} />
    
  </Switch>
</BrowserRouter>
</AuthContext.Provider>
);
}

ReactDOM.render(
  <React.StrictMode>
  <AppRender/>
  </React.StrictMode>
,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
