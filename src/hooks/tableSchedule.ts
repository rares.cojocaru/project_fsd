import { AxiosError } from "axios";
import instance from "../api/gcpInstance"

export default {
    async addSchedule(start_date: string, end_date: string, month: string): Promise<any> {
        try {
            const resp = await instance.get("add", {
                params: {
                    "start_date": start_date+" "+ month + " 2022",
                    "end_date": end_date+" "+ month + " 2022"
                }
            });
            console.log(resp)
            return resp.data;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status)
                console.log(err.response.data)
            }
            throw error;
        }
    },

    async getSchedule() {
        try {
            const resp = await instance.get("get");
            return resp;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status);
                console.log(err.response.data);
            }
            throw error;
        }
    }
}

