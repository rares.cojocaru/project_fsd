import { AxiosError } from "axios";
import instance from "../api/instance"

export default {
    async login(email: string, password: string): Promise<any> {
        try{
            const resp = await instance.post("/users/login", {email, password});
            console.log(resp)
            return resp.data;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status)
                console.log(err.response.data)
            }
            throw error;
        }
    },

    logout() {
        localStorage.removeItem("token");
    },

    async getUserInfo() {
        try {
            const resp = await instance.get("/users/info");
            return resp.data;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status);
                console.log(err.response.data);
            }
            throw error;
        }
    }
}
