import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import tableSchedule from './hooks/tableSchedule';
import { useState, useEffect } from 'react';


export const rows = [
    {
        id: 1,
        start_date: 2,
        end_date: 1
    },
    {
        id: 2,
        start_date: 4,
        end_date: 3
    },
]

function createData(start_date: string, end_date: string) {
    return {
        start_date,
        end_date
    };
}


const TableSchedule = () => {

    const [data, setData] = useState([
        {
            start_date: null,
            end_date: null
        }
    ])
    useEffect(() => {
        async function getData() {
            const schedules = await tableSchedule.getSchedule();
            const newImages = schedules.data.map((image: any) => createData(image.start_date, image.end_date));
            setData(schedules.data)
            console.log(data)
        }
        getData();
    },[data])

    return (
        <div style={styles.table}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Start Date</TableCell>
                            <TableCell>End Date</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow
                                key={row.start_date}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell>{row.start_date}</TableCell>
                                <TableCell>{row.end_date}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

const styles = {
    table: {
        width: '40%',
        marginTop: '10%',
        marginLeft: '2%'
    },
}

export default TableSchedule