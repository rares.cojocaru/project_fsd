import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { useState, useEffect } from "react";
import  useLocalStorage from './hooks/uploadImage';
import instance from "./api/instance";


const FileInput = () => {

    async function getURL(url: string) {
        try {
          const response = await fetch(url, {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
              "Content-Type": "image/png",
            },
          });
          const blob = await response.blob();
          return [URL.createObjectURL(blob), null];
        } catch (error) {
          console.error(`get: error occurred ${error}`);
          return [null, error];
        }
      }
     
  const [selectedImage, setSelectedImage] = useState<Blob>();
  const [imageUrl, setImageUrl] = useState("");
  const [images, setImages] = useLocalStorage("images", JSON.stringify([]));
  const [file, setFile] = useState<File>();

  useEffect(() => {
    async function fetchData() {
      if (selectedImage) {
        var formData = new FormData();
        formData.append("image", selectedImage);
        instance.post("evaluate", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        setImageUrl(URL.createObjectURL(selectedImage));
      }
    }
    fetchData();
  }, [selectedImage]);
    return (
        <>
            <input
                accept="image/*"
                type="file"
                id="select-image"
                style={{ display: 'none' }}
                onChange={(e) => setSelectedImage(e.target.files![0])}
            />
        
            <Box mt={5} ml={30} bgcolor={'#eeeeee'} width={'900px'} height={'550px'}>
                <label htmlFor="select-image">
                    <Button variant="contained" component="span" style={{ marginLeft: "750px", marginTop: "20px", backgroundColor:"#C0C0C0"}}>
                        <b>Upload</b>
                    </Button>
                </label>
                <Box mt={3} ml={6} fontSize="30px" color="black" textAlign="center" bgcolor={'#C0C0C0'} width={'800px'} height={'400px'}>
                <div style={{ paddingTop: "150px" }} >
                    <img src={imageUrl} style={{ paddingTop: "100px" }} width="650px" height="350px" >

                    </img>
                </div>
                </Box>
            </Box>
            
        </>
    );
};

export default FileInput;