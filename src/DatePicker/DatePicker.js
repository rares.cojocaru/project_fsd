import { Paper } from '@mui/material';
import { Component } from 'react'
import './DatePicker.css';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import tableSchedule from '../hooks/tableSchedule';

export const theme_bac = {
    background: "linear-gradient(135deg, #9900ff, #0099cc)",
    height: "8vh",
    width: "27vw"
};

export const theme_bac1 = {
    background: "linear-gradient(135deg,  #0099cc, #9900ff)",
    width: "27vw"
};

export const months = {
    jan : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", 1, 2, 3, 4, 5, 6],
        [7, 8, 9, 10, 11, 12, 13],
        [14, 15, 16, 17, 18, 19, 20],
        [21, 22, 23, 24, 25, 26, 27],
        [28, 29, 30, 31, "", "", ""]
        ],
    feb : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", "", 1, 2, 3],
        [4, 5, 6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15, 16, 17],
        [18, 19, 20, 21, 22, 23, 24],
        [25, 26, 27, 28, "", "", ""]
        ],
    mar : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", "", 1, 2, 3],
        [4, 5, 6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15, 16, 17],
        [18, 19, 20, 21, 22, 23, 24],
        [25, 26, 27, 28, 29, 30, 31]
        ],
    apr : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    may : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    jun : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    jul : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    aug : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    sep : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    oct : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    nov : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ],
    dec : [
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        ["", "", "", 1, 2, 3, 4],
        [5, 6, 7, 8, 9, 10, 11],
        [12, 13, 14, 15, 16, 17, 18],
        [19, 20, 21, 22, 23, 24, 25],
        [26, 27, 28, 29, 30, 31, ""]
        ]
};


class DatePicker extends Component {
    constructor() {
        super();
        this.state = {
            board: [
                ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
                ["", "", "", 1, 2, 3, 4],
                [5, 6, 7, 8, 9, 10, 11],
                [12, 13, 14, 15, 16, 17, 18],
                [19, 20, 21, 22, 23, 24, 25],
                [26, 27, 28, 29, 30, 31, ""]
                ],
            showCalendar: false,
            start_date: "1",
            end_date: "20",
            bool: false,
            months: ['JAN', 'FEB', 'MAR', 'APR',
                'MAY', 'JUN', 'JUL', 'AUG',
                'SEP', 'OCT', 'NOV', 'DEC'],
            month: 'JAN'
        };
        this.hideComponent = this.hideComponent.bind(this);

    }
    hideComponent() {
        this.setState({ showCalendar: !this.state.showCalendar });
    }
    displayCalendar() {
        this.hideComponent();
    }

    handleDays(col) {
        if (this.state['bool'] === false) {
            this.setState({ start_date: col, bool: true });
        } else {
            this.setState({ end_date: col, bool: false });
        }
    }

    handleMonth(month) {
        this.setState({ month: month });
    }

    addSchedule(){
        tableSchedule.addSchedule(this.state['start_date'], this.state['end_date'],this.state['month']);
    }

    render() {
        const { board, showCalendar, start_date, end_date, months, month } = this.state;

        return (
            <div>
                <div className="label" style={{ fontSize: "50px" }}>Date Picker</div>
                <div className="border">
                    <Grid container spacing={3}
                        direction="row"
                        justifyContent="space-around"
                        alignItems="center">
                        <Grid item>
                            <button className="startDateButton" onClick={() => this.hideComponent()}>
                                Start Date
                            </button>
                            <div style={{ color: "#cc00ff", fontWeight: "900", opacity: "0.6" }}>
                                {start_date} {month} 2022
                            </div>
                        </Grid>
                        <Grid item>
                            <div className="vLine"></div>
                        </Grid>
                        <Grid item>
                            <div>
                                End Date
                            </div>
                            <div style={{ color: "#0099cc", fontWeight: "900", opacity: "0.6" }}>
                                {end_date} {month} 2022
                            </div>
                        </Grid>
                    </Grid>
                </div>
                <div>
                    <div class="arrow-up"></div>
                    {showCalendar && (
                        <Paper elevation={3} className="calendar">

                            <Grid container
                                direction="row"
                                justifyContent="space-evenly"
                                marginTop="-140px"
                                alignItems="center" style={{ ...theme_bac }}>
                                <Grid item color="white">
                                    15 Today
                                </Grid>
                                <Grid item color="white">
                                    <Select value={month} >
                                        {months?.map(month => {
                                            return (
                                                <MenuItem key={month} value={month} onClick={() => this.handleMonth(month)}>
                                                    {month}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </Grid>
                                <Grid item color="white">
                                    2022
                                </Grid>
                            </Grid>
                            <Divider className="divider" />
                            <table style={{ ...theme_bac1 }}>
                                <tbody>
                                    {board.map((row, i) => (
                                        <tr key={i}>
                                            {row.map((col, j) => (
                                                <td key={j} >
                                                    <button id="btn" onClick={() => this.handleDays(col)} className="cell"  >
                                                        {col}
                                                    </button>
                                                </td>
                                            ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            <Divider />
                            <Divider className="divider2"></Divider>
                            <Grid container spacing={3}
                                className="buttonsContainer"
                                alignItems="center"
                                direction="row"
                                justifyContent="center"
                            >
                                <Grid item>
                                    <Button onClick={() => {this.addSchedule();this.hideComponent()}} className="butt" variant="contained">Done</Button>
                                </Grid>
                                <Grid item>
                                    <Button onClick={() => {this.hideComponent()}} className="butt" variant="contained">Cancel</Button>
                                </Grid>
                            </Grid>

                        </Paper>

                    )}

                </div>
            </div>
        );
    }
};

export default DatePicker